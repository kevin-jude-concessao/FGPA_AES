#!/bin/sh -f
xv_path="/opt/Xilinx/Vivado/2014.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim AES_tb_behav -key {Behavioral:sim_1:Functional:AES_tb} -tclbatch AES_tb.tcl -log simulate.log
