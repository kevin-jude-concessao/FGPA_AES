#!/bin/sh -f
xv_path="/opt/Xilinx/Vivado/2014.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto 43bb97ae0bb44d62a7e81660c1065867 -m64 --debug typical --relax --include "../../../AES.srcs/sources_1/ip/AES_IO/ltlib_v1_0/hdl/verilog" --include "../../../AES.srcs/sources_1/ip/AES_IO/vio_v3_0/hdl" --include "../../../AES.srcs/sources_1/ip/AES_IO/xsdbs_v1_0/hdl/verilog" -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip --snapshot AES_tb_behav xil_defaultlib.AES_tb xil_defaultlib.glbl -log elaborate.log
