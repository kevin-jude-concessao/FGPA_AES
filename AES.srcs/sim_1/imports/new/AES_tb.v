`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.04.2019 18:30:54
// Design Name: 
// Module Name: AES_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module AES_tb();
reg [0:127] test_bytes;
reg [0:127] test_key;
reg [0:127] test_output;

InverseAES encrypt(.Input(test_bytes), .Key(test_key), .Output(test_output));
//MixColumns imc(test_bytes, test_key);
//InverseMixColumns imc_1(test_bytes, test_output);
initial
begin
   test_bytes <= 128'hff0b844a0853bf7c6934ab4364148fb9;
   test_key <= 128'h0f1571c947d9e8590cb7add6af7f6798;
end
endmodule