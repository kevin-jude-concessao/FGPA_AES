// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.4 (lin64) Build 1071353 Tue Nov 18 16:47:07 MST 2014
// Date        : Wed Apr 24 00:44:24 2019
// Host        : Ideapad-320 running 64-bit Ubuntu 19.04
// Command     : write_verilog -force -mode synth_stub /home/jude/Downloads/AES/AES.srcs/sources_1/ip/AES_IO/AES_IO_stub.v
// Design      : AES_IO
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2014.4" *)
module AES_IO(clk, probe_in0, probe_in1, probe_out0, probe_out1)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_in0[127:0],probe_in1[127:0],probe_out0[127:0],probe_out1[127:0]" */;
  input clk;
  input [127:0]probe_in0;
  input [127:0]probe_in1;
  output [127:0]probe_out0;
  output [127:0]probe_out1;
endmodule
