-- Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2014.4 (lin64) Build 1071353 Tue Nov 18 16:47:07 MST 2014
-- Date        : Wed Apr 24 00:44:24 2019
-- Host        : Ideapad-320 running 64-bit Ubuntu 19.04
-- Command     : write_vhdl -force -mode synth_stub /home/jude/Downloads/AES/AES.srcs/sources_1/ip/AES_IO/AES_IO_stub.vhdl
-- Design      : AES_IO
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity AES_IO is
  Port ( 
    clk : in STD_LOGIC;
    probe_in0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
    probe_in1 : in STD_LOGIC_VECTOR ( 127 downto 0 );
    probe_out0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
    probe_out1 : out STD_LOGIC_VECTOR ( 127 downto 0 )
  );

end AES_IO;

architecture stub of AES_IO is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,probe_in0[127:0],probe_in1[127:0],probe_out0[127:0],probe_out1[127:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "vio,Vivado 2014.4";
begin
end;
