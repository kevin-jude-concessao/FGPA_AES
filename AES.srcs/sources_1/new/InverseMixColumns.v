`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2019 21:59:56
// Design Name: 
// Module Name: InverseMixColumns
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module InverseMixColumns(Input, Output);
	input  [0:127] Input;
	output [0:127] Output;
	
	wire [0:7] Wires  [32];
	wire [0:7] Wires_1[32];
	
	GaloisMultiplication mult_00(.A(8'h0E), .B(Input[0:7]),  .P(Wires[0]));
    GaloisMultiplication mult_01(.A(8'h0E), .B(Input[8:15]), .P(Wires[1]));
    GaloisMultiplication mult_02(.A(8'h0E), .B(Input[16:23]),.P(Wires[2]));
    GaloisMultiplication mult_03(.A(8'h0E), .B(Input[24:31]),.P(Wires[3]));
    
    GaloisMultiplication mult_04(.A(8'h0B), .B(Input[32:39]), .P(Wires[4]));
    GaloisMultiplication mult_05(.A(8'h0B), .B(Input[40:47]), .P(Wires[5]));
    GaloisMultiplication mult_06(.A(8'h0B), .B(Input[48:55]), .P(Wires[6]));
    GaloisMultiplication mult_07(.A(8'h0B), .B(Input[56:63]), .P(Wires[7]));
    
    GaloisMultiplication mult_10(.A(8'h0E), .B(Input[32:39]), .P(Wires[8]));
    GaloisMultiplication mult_11(.A(8'h0E), .B(Input[40:47]), .P(Wires[9]));
    GaloisMultiplication mult_12(.A(8'h0E), .B(Input[48:55]), .P(Wires[10]));
    GaloisMultiplication mult_13(.A(8'h0E), .B(Input[56:63]), .P(Wires[11]));
    
    GaloisMultiplication mult_14(.A(8'h0B), .B(Input[64:71]), .P(Wires[12]));
    GaloisMultiplication mult_15(.A(8'h0B), .B(Input[72:79]), .P(Wires[13]));
    GaloisMultiplication mult_16(.A(8'h0B), .B(Input[80:87]), .P(Wires[14]));
    GaloisMultiplication mult_17(.A(8'h0B), .B(Input[88:95]), .P(Wires[15]));
    
    GaloisMultiplication mult_20(.A(8'h0E), .B(Input[64:71]), .P(Wires[16]));
    GaloisMultiplication mult_21(.A(8'h0E), .B(Input[72:79]), .P(Wires[17]));
    GaloisMultiplication mult_22(.A(8'h0E), .B(Input[80:87]), .P(Wires[18]));
    GaloisMultiplication mult_23(.A(8'h0E), .B(Input[88:95]), .P(Wires[19]));
    
    GaloisMultiplication mult_24(.A(8'h0B), .B(Input[96:103]),  .P(Wires[20]));
    GaloisMultiplication mult_25(.A(8'h0B), .B(Input[104:111]), .P(Wires[21]));
    GaloisMultiplication mult_26(.A(8'h0B), .B(Input[112:119]), .P(Wires[22]));
    GaloisMultiplication mult_27(.A(8'h0B), .B(Input[120:127]), .P(Wires[23]));    
    
    GaloisMultiplication mult_30(.A(8'h0E), .B(Input[96:103]),  .P(Wires[24]));
    GaloisMultiplication mult_31(.A(8'h0E), .B(Input[104:111]), .P(Wires[25]));
    GaloisMultiplication mult_32(.A(8'h0E), .B(Input[112:119]), .P(Wires[26]));
    GaloisMultiplication mult_33(.A(8'h0E), .B(Input[120:127]), .P(Wires[27]));
    
    GaloisMultiplication mult_34(.A(8'h0B), .B(Input[0:7]),   .P(Wires[28]));
    GaloisMultiplication mult_35(.A(8'h0B), .B(Input[8:15]),  .P(Wires[29]));
    GaloisMultiplication mult_36(.A(8'h0B), .B(Input[16:23]), .P(Wires[30]));
    GaloisMultiplication mult_37(.A(8'h0B), .B(Input[24:31]), .P(Wires[31]));
    
    // ---------------------------------------------------------------------------- //
    
    GaloisMultiplication mult_40(.A(8'h0D), .B(Input[64:71]), .P(Wires_1[0]));
    GaloisMultiplication mult_41(.A(8'h0D), .B(Input[72:79]), .P(Wires_1[1]));
    GaloisMultiplication mult_42(.A(8'h0D), .B(Input[80:87]), .P(Wires_1[2]));
    GaloisMultiplication mult_43(.A(8'h0D), .B(Input[88:95]), .P(Wires_1[3]));
    
    GaloisMultiplication mult_44(.A(8'h09), .B(Input[96:103]),  .P(Wires_1[4]));
    GaloisMultiplication mult_45(.A(8'h09), .B(Input[104:111]), .P(Wires_1[5]));
    GaloisMultiplication mult_46(.A(8'h09), .B(Input[112:119]), .P(Wires_1[6]));
    GaloisMultiplication mult_47(.A(8'h09), .B(Input[120:127]), .P(Wires_1[7]));
    
    GaloisMultiplication mult_50(.A(8'h0D), .B(Input[96:103]), .P(Wires_1[8]));
    GaloisMultiplication mult_51(.A(8'h0D), .B(Input[104:111]), .P(Wires_1[9]));
    GaloisMultiplication mult_52(.A(8'h0D), .B(Input[112:119]), .P(Wires_1[10]));
    GaloisMultiplication mult_53(.A(8'h0D), .B(Input[120:127]), .P(Wires_1[11]));
    
    GaloisMultiplication mult_54(.A(8'h09), .B(Input[0:7]), .P(Wires_1[12]));
    GaloisMultiplication mult_55(.A(8'h09), .B(Input[8:15]), .P(Wires_1[13]));
    GaloisMultiplication mult_56(.A(8'h09), .B(Input[16:23]), .P(Wires_1[14]));
    GaloisMultiplication mult_57(.A(8'h09), .B(Input[24:31]), .P(Wires_1[15]));
    
    GaloisMultiplication mult_60(.A(8'h0D), .B(Input[0:7]), .P(Wires_1[16]));
    GaloisMultiplication mult_61(.A(8'h0D), .B(Input[8:15]), .P(Wires_1[17]));
    GaloisMultiplication mult_62(.A(8'h0D), .B(Input[16:23]), .P(Wires_1[18]));
    GaloisMultiplication mult_63(.A(8'h0D), .B(Input[24:31]), .P(Wires_1[19]));
    
    GaloisMultiplication mult_64(.A(8'h09), .B(Input[32:39]),  .P(Wires_1[20]));
    GaloisMultiplication mult_65(.A(8'h09), .B(Input[40:47]), .P(Wires_1[21]));
    GaloisMultiplication mult_66(.A(8'h09), .B(Input[48:55]), .P(Wires_1[22]));
    GaloisMultiplication mult_67(.A(8'h09), .B(Input[46:63]), .P(Wires_1[23]));    
    
    GaloisMultiplication mult_70(.A(8'h0D), .B(Input[32:39]),  .P(Wires_1[24]));
    GaloisMultiplication mult_71(.A(8'h0D), .B(Input[40:47]), .P(Wires_1[25]));
    GaloisMultiplication mult_72(.A(8'h0D), .B(Input[48:55]), .P(Wires_1[26]));
    GaloisMultiplication mult_73(.A(8'h0D), .B(Input[56:63]), .P(Wires_1[27]));
    
    GaloisMultiplication mult_74(.A(8'h09), .B(Input[64:71]),   .P(Wires_1[28]));
    GaloisMultiplication mult_75(.A(8'h09), .B(Input[72:79]),  .P(Wires_1[29]));
    GaloisMultiplication mult_76(.A(8'h09), .B(Input[80:87]), .P(Wires_1[30]));
    GaloisMultiplication mult_77(.A(8'h09), .B(Input[88:95]), .P(Wires_1[31]));
    
    
	assign Output[0:7]    =  Wires[0] ^ Wires[4] ^ Wires_1[0] ^ Wires_1[4];
	assign Output[8:15]   =  Wires[1] ^ Wires[5] ^ Wires_1[1] ^ Wires_1[5];
	assign Output[16:23]  =  Wires[2] ^ Wires[6] ^ Wires_1[2] ^ Wires_1[6];
	assign Output[24:31]  =  Wires[3] ^ Wires[7] ^ Wires_1[3] ^ Wires_1[7];
	
	assign Output[32:39]  =  Wires_1[12] ^ Wires[8] ^ Wires[12] ^ Wires_1[8];
	assign Output[40:47]  =  Wires_1[13] ^ Wires[9] ^ Wires[13] ^ Wires_1[9]; 
	assign Output[48:55]  =  Wires_1[14] ^ Wires[10] ^ Wires[14] ^ Wires_1[10];
	assign Output[56:63]  =  Wires_1[15] ^ Wires[11] ^ Wires[15] ^ Wires_1[11];
	
	assign Output[64:71]  =  Wires_1[16] ^ Wires_1[20] ^ Wires[16] ^ Wires[20];
	assign Output[72:79]  =  Wires_1[17] ^ Wires_1[21] ^ Wires[17] ^ Wires[21];
	assign Output[80:87]  =  Wires_1[18] ^ Wires_1[22] ^ Wires[18] ^ Wires[22];
	assign Output[88:95]  =  Wires_1[19] ^ Wires_1[23] ^ Wires[19] ^ Wires[23];
	
	assign Output[96:103] =  Wires[28] ^ Wires_1[24] ^ Wires_1[28] ^ Wires[24];
	assign Output[104:111]=  Wires[29] ^ Wires_1[25] ^ Wires_1[29] ^ Wires[25];
	assign Output[112:119]=  Wires[30] ^ Wires_1[26] ^ Wires_1[30] ^ Wires[26];
	assign Output[120:127]=  Wires[31] ^ Wires_1[27] ^ Wires_1[31] ^ Wires[27];
	
endmodule
