`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2019 22:00:18
// Design Name: 
// Module Name: InverseAES
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module InverseAES(Input, Key, Output);
input  [0:127] Input;
input  [0:127] Key;
output [0:127] Output;

wire [0:127] ExpandedKey[0:10];

wire [0:127] aic_0_out;
wire [0:127] aic_1_out;
wire [0:127] aic_2_out;
wire [0:127] aic_3_out;
wire [0:127] aic_4_out;
wire [0:127] aic_5_out;
wire [0:127] aic_6_out;
wire [0:127] aic_7_out;
wire [0:127] aic_8_out;
wire [0:127] aic_9_out;
wire [0:127] aic_10_out;
wire [0:127] aic_out;

wire [0:127] ark_0_out;
wire [0:127] ark_1_out;
wire [0:127] ark_2_out;
wire [0:127] ark_3_out;
wire [0:127] ark_4_out;
wire [0:127] ark_5_out;
wire [0:127] ark_6_out;
wire [0:127] ark_7_out;
wire [0:127] ark_8_out;
wire [0:127] ark_9_out;
wire [0:127] ark_10_out;

wire [0:127] sb_1_out;
wire [0:127] sb_2_out;
wire [0:127] sb_3_out;
wire [0:127] sb_4_out;
wire [0:127] sb_5_out;
wire [0:127] sb_6_out;
wire [0:127] sb_7_out;
wire [0:127] sb_8_out;
wire [0:127] sb_9_out;
wire [0:127] sb_10_out;

wire [0:127] sr_1_out;
wire [0:127] sr_2_out;
wire [0:127] sr_3_out;
wire [0:127] sr_4_out;
wire [0:127] sr_5_out;
wire [0:127] sr_6_out;
wire [0:127] sr_7_out;
wire [0:127] sr_8_out;
wire [0:127] sr_9_out;
wire [0:127] sr_10_out;

wire [0:127] mc_1_out;
wire [0:127] mc_2_out;
wire [0:127] mc_3_out;
wire [0:127] mc_4_out;
wire [0:127] mc_5_out;
wire [0:127] mc_6_out;
wire [0:127] mc_7_out;
wire [0:127] mc_8_out;
wire [0:127] mc_9_out;

wire [0:127] kmc_1_out;
wire [0:127] kmc_2_out;
wire [0:127] kmc_3_out;
wire [0:127] kmc_4_out;
wire [0:127] kmc_5_out;
wire [0:127] kmc_6_out;
wire [0:127] kmc_7_out;
wire [0:127] kmc_8_out;
wire [0:127] kmc_9_out;

assign ExpandedKey[0] = Key;

KeyExpansion ke_1(.OldKey(ExpandedKey[0]), .Round(8'b0000_0001), .NewKey(ExpandedKey[1]));
KeyExpansion ke_2(.OldKey(ExpandedKey[1]), .Round(8'b0000_0010), .NewKey(ExpandedKey[2]));
KeyExpansion ke_3(.OldKey(ExpandedKey[2]), .Round(8'b0000_0011), .NewKey(ExpandedKey[3]));
KeyExpansion ke_4(.OldKey(ExpandedKey[3]), .Round(8'b0000_0100), .NewKey(ExpandedKey[4]));
KeyExpansion ke_5(.OldKey(ExpandedKey[4]), .Round(8'b0000_0101), .NewKey(ExpandedKey[5]));
KeyExpansion ke_6(.OldKey(ExpandedKey[5]), .Round(8'b0000_0110), .NewKey(ExpandedKey[6]));
KeyExpansion ke_7(.OldKey(ExpandedKey[6]), .Round(8'b0000_0111), .NewKey(ExpandedKey[7]));
KeyExpansion ke_8(.OldKey(ExpandedKey[7]), .Round(8'b0000_1000), .NewKey(ExpandedKey[8]));
KeyExpansion ke_9(.OldKey(ExpandedKey[8]), .Round(8'b0000_1001), .NewKey(ExpandedKey[9]));
KeyExpansion ke_10(.OldKey(ExpandedKey[9]), .Round(8'b0000_1010), .NewKey(ExpandedKey[10]));

ArrangeIntoColumns aic(.Input(Input), .Output(aic_out));

ArrangeIntoColumns aic_0(.Input(ExpandedKey[10]), .Output(aic_0_out));
AddRoundKey ark_0(.StateMatrix(aic_out), .RoundKey(aic_0_out), .OutputMatrix(ark_0_out));

InverseSubstituteBytes sb_1(.Input(ark_0_out), .Output(sb_1_out));
InverseShiftRows sr_1(.InputRows(sb_1_out), .TransformedRows(sr_1_out));
InverseMixColumns mc_1(.Input(sr_1_out), .Output(mc_1_out));
ArrangeIntoColumns aic_1(.Input(ExpandedKey[9]), .Output(aic_1_out));
InverseMixColumns kmc_1(.Input(aic_1_out), .Output(kmc_1_out));
AddRoundKey ark_1(.StateMatrix(mc_1_out), .RoundKey(kmc_1_out), .OutputMatrix(ark_1_out));

InverseSubstituteBytes sb_2(.Input(ark_1_out), .Output(sb_2_out));
InverseShiftRows sr_2(.InputRows(sb_2_out), .TransformedRows(sr_2_out));
InverseMixColumns mc_2(.Input(sr_2_out), .Output(mc_2_out));
ArrangeIntoColumns aic_2(.Input(ExpandedKey[8]), .Output(aic_2_out));
InverseMixColumns kmc_2(.Input(aic_2_out), .Output(kmc_2_out));
AddRoundKey ark_2(.StateMatrix(mc_2_out), .RoundKey(kmc_2_out), .OutputMatrix(ark_2_out));

InverseSubstituteBytes sb_3(.Input(ark_2_out), .Output(sb_3_out));
InverseShiftRows sr_3(.InputRows(sb_3_out), .TransformedRows(sr_3_out));
InverseMixColumns mc_3(.Input(sr_3_out), .Output(mc_3_out));
ArrangeIntoColumns aic_3(.Input(ExpandedKey[7]), .Output(aic_3_out));
InverseMixColumns kmc_3(.Input(aic_3_out), .Output(kmc_3_out));
AddRoundKey ark_3(.StateMatrix(mc_3_out), .RoundKey(kmc_3_out), .OutputMatrix(ark_3_out));

InverseSubstituteBytes sb_4(.Input(ark_3_out), .Output(sb_4_out));
InverseShiftRows sr_4(.InputRows(sb_4_out), .TransformedRows(sr_4_out));
InverseMixColumns mc_4(.Input(sr_4_out), .Output(mc_4_out));
ArrangeIntoColumns aic_4(.Input(ExpandedKey[6]), .Output(aic_4_out));
InverseMixColumns kmc_4(.Input(aic_4_out), .Output(kmc_4_out));
AddRoundKey ark_4(.StateMatrix(mc_4_out), .RoundKey(kmc_4_out), .OutputMatrix(ark_4_out));

InverseSubstituteBytes sb_5(.Input(ark_4_out), .Output(sb_5_out));
InverseShiftRows sr_5(.InputRows(sb_5_out), .TransformedRows(sr_5_out));
InverseMixColumns mc_5(.Input(sr_5_out), .Output(mc_5_out));
ArrangeIntoColumns aic_5(.Input(ExpandedKey[5]), .Output(aic_5_out));
InverseMixColumns kmc_5(.Input(aic_5_out), .Output(kmc_5_out));
AddRoundKey ark_5(.StateMatrix(mc_5_out), .RoundKey(kmc_5_out), .OutputMatrix(ark_5_out));

InverseSubstituteBytes sb_6(.Input(ark_5_out), .Output(sb_6_out));
InverseShiftRows sr_6(.InputRows(sb_6_out), .TransformedRows(sr_6_out));
InverseMixColumns mc_6(.Input(sr_6_out), .Output(mc_6_out));
ArrangeIntoColumns aic_6(.Input(ExpandedKey[4]), .Output(aic_6_out));
InverseMixColumns kmc_6(.Input(aic_6_out), .Output(kmc_6_out));
AddRoundKey ark_6(.StateMatrix(mc_6_out), .RoundKey(kmc_6_out), .OutputMatrix(ark_6_out));

InverseSubstituteBytes sb_7(.Input(ark_6_out), .Output(sb_7_out));
InverseShiftRows sr_7(.InputRows(sb_7_out), .TransformedRows(sr_7_out));
InverseMixColumns mc_7(.Input(sr_7_out), .Output(mc_7_out));
ArrangeIntoColumns aic_7(.Input(ExpandedKey[3]), .Output(aic_7_out));
InverseMixColumns kmc_7(.Input(aic_7_out), .Output(kmc_7_out));
AddRoundKey ark_7(.StateMatrix(mc_7_out), .RoundKey(kmc_7_out), .OutputMatrix(ark_7_out));

InverseSubstituteBytes sb_8(.Input(ark_7_out), .Output(sb_8_out));
InverseShiftRows sr_8(.InputRows(sb_8_out), .TransformedRows(sr_8_out));
InverseMixColumns mc_8(.Input(sr_8_out), .Output(mc_8_out));
ArrangeIntoColumns aic_8(.Input(ExpandedKey[2]), .Output(aic_8_out));
InverseMixColumns kmc_8(.Input(aic_8_out), .Output(kmc_8_out));
AddRoundKey ark_8(.StateMatrix(mc_8_out), .RoundKey(kmc_8_out), .OutputMatrix(ark_8_out));

InverseSubstituteBytes sb_9(.Input(ark_8_out), .Output(sb_9_out));
InverseShiftRows sr_9(.InputRows(sb_9_out), .TransformedRows(sr_9_out));
InverseMixColumns mc_9(.Input(sr_9_out), .Output(mc_9_out));
ArrangeIntoColumns aic_9(.Input(ExpandedKey[1]), .Output(aic_9_out));
InverseMixColumns kmc_9(.Input(aic_9_out), .Output(kmc_9_out));
AddRoundKey ark_9(.StateMatrix(mc_9_out), .RoundKey(kmc_9_out), .OutputMatrix(ark_9_out));

InverseSubstituteBytes sb_10(.Input(ark_9_out), .Output(sb_10_out));
InverseShiftRows sr_10(.InputRows(sb_10_out), .TransformedRows(sr_10_out));
ArrangeIntoColumns aic_10(.Input(ExpandedKey[0]), .Output(aic_10_out));
AddRoundKey ark_10(.StateMatrix(sr_10_out), .RoundKey(aic_10_out), .OutputMatrix(ark_10_out));
ArrangeIntoRows air(.Input(ark_10_out), .Output(Output));

endmodule
