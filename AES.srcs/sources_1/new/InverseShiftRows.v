`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2019 22:00:18
// Design Name: 
// Module Name: InverseAES
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module InverseShiftRows(InputRows, TransformedRows);
input  [0:127] InputRows;
output [0:127] TransformedRows;
assign TransformedRows[0:7]   = InputRows[0:7];
assign TransformedRows[8:15]  = InputRows[8:15];
assign TransformedRows[16:23] = InputRows[16:23];
assign TransformedRows[24:31] = InputRows[24:31];

assign TransformedRows[32:39]  = InputRows[56:63];
assign TransformedRows[40:47]  = InputRows[32:39];
assign TransformedRows[48:55]  = InputRows[40:47];
assign TransformedRows[56:63]  = InputRows[48:55];

assign TransformedRows[64:71]  = InputRows[80:87];
assign TransformedRows[72:79]  = InputRows[88:95];
assign TransformedRows[80:87]  = InputRows[64:71];
assign TransformedRows[88:95]  = InputRows[72:79];

assign TransformedRows[96:103]    = InputRows[104:111];
assign TransformedRows[104:111]   = InputRows[112:119];
assign TransformedRows[112:119]   = InputRows[120:127];
assign TransformedRows[120:127]   = InputRows[96:103];
endmodule
