`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.04.2019 16:49:31
// Design Name: 
// Module Name: AddRoundKey
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module AddRoundKey(StateMatrix, RoundKey, OutputMatrix);
input  [0:127] StateMatrix;
input  [0:127] RoundKey;
output [0:127] OutputMatrix;
assign OutputMatrix[0:31]  = StateMatrix[0:31]  ^ RoundKey[0:31];
assign OutputMatrix[32:63] = StateMatrix[32:63] ^ RoundKey[32:63];
assign OutputMatrix[64:95] = StateMatrix[64:95] ^ RoundKey[64:95];
assign OutputMatrix[96:127] = StateMatrix[96:127] ^ RoundKey[96:127];
endmodule
