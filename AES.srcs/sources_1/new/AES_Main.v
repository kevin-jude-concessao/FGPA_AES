`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.04.2019 11:45:30
// Design Name: 
// Module Name: AES_Main
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module AES_Main(Clock);
input Clock;

wire [0:127] Input;
wire [0:127] Key;
wire [0:127] Output_Encryption;
wire [0:127] Output_Decryption;

AES Encryption(.Input(Input), .Key(Key), .Output(Output_Encryption));
InverseAES Decryption(.Input(Output_Encryption), .Key(Key), .Output(OutputDecryption));      
AES_IO IO(Clock, Output_Encryption, OutputDecryption, Input, Key);

endmodule
