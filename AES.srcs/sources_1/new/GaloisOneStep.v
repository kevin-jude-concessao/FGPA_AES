`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2019 19:11:07
// Design Name: 
// Module Name: GaloisOneStep
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GaloisOneStep(OldA, OldB, OldP, NewA, NewB, NewP);
input [0:7] OldA;
input [0:7] OldB;
input [0:7] OldP;
output [0:7] NewA;
output [0:7] NewB;
output [0:7] NewP;

assign NewP = ((OldB & 8'h01) == 8'h01) ? (OldP ^ OldA) : (OldP);
assign NewA = ((OldA & 8'h80) == 8'h80) ? ((OldA << 1) ^ 8'h1b) : (OldA << 1);
assign NewB = (OldB >> 1);

endmodule