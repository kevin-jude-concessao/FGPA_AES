`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.04.2019 16:51:23
// Design Name: 
// Module Name: KeyExpansion
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module KeyExpansion(OldKey, Round, NewKey);
	input  [0:127] OldKey;
	input  [0:7]   Round;
 	output [0:127] NewKey;
 	reg    [0:31]  rotated;
 	assign rotated = RotateWord(OldKey[96:127]);
 	assign NewKey[0:31]   = OldKey[0:31]   ^ RCon(Round) ^ {SBox(rotated[0:7]), SBox(rotated[8:15]), SBox(rotated[16:23]), SBox(rotated[24:31])};
    assign NewKey[32:63]  = OldKey[32:63]  ^ NewKey[0:31];
    assign NewKey[64:95]  = OldKey[64:95]  ^ NewKey[32:63];
    assign NewKey[96:127] = OldKey[96:127] ^ NewKey[34:95];
    
 	function [0:31] RotateWord;
     input [0:31] word;
     begin
         RotateWord = { word[8:15], word[16:23], word[24:31], word[0:7] };
     end
     endfunction     
     
     function [0:31] RCon;
     input [0:7] index;
     begin
         case (index)
             8'd1:  RCon = { 8'b00000001, 8'b00000000, 8'b00000000, 8'b00000000 };
             8'd2:  RCon = { 8'b00000010, 8'b00000000, 8'b00000000, 8'b00000000 };
             8'd3:  RCon = { 8'b00000100, 8'b00000000, 8'b00000000, 8'b00000000 };
             8'd4:  RCon = { 8'b00001000, 8'b00000000, 8'b00000000, 8'b00000000 };
             8'd5:  RCon = { 8'b00010000, 8'b00000000, 8'b00000000, 8'b00000000 };
             8'd6:  RCon = { 8'b00100000, 8'b00000000, 8'b00000000, 8'b00000000 };
             8'd7:  RCon = { 8'b01000000, 8'b00000000, 8'b00000000, 8'b00000000 };
             8'd8:  RCon = { 8'b10000000, 8'b00000000, 8'b00000000, 8'b00000000 };
             8'd9:  RCon = { 8'b00011011, 8'b00000000, 8'b00000000, 8'b00000000 };
             8'd10: RCon = { 8'b00110110, 8'b00000000, 8'b00000000, 8'b00000000 };
         endcase
     end
     endfunction
     
     function [0:7] SBox;
     input [0:7] select;
     begin
         case(select)
             8'b00000000 : SBox = 8'b01100011;
             8'b00000001 : SBox = 8'b01111100;
             8'b00000010 : SBox = 8'b01110111;
             8'b00000011 : SBox = 8'b01111011;
             8'b00000100 : SBox = 8'b11110010;
             8'b00000101 : SBox = 8'b01101011;
             8'b00000110 : SBox = 8'b01101111;
             8'b00000111 : SBox = 8'b11000101;
             8'b00001000 : SBox = 8'b00110000;
             8'b00001001 : SBox = 8'b00000001;
             8'b00001010 : SBox = 8'b01100111;
             8'b00001011 : SBox = 8'b00101011;
             8'b00001100 : SBox = 8'b11111110;
             8'b00001101 : SBox = 8'b11010111;
             8'b00001110 : SBox = 8'b10101011;
             8'b00001111 : SBox = 8'b01110110;
             8'b00010000 : SBox = 8'b11001010;
             8'b00010001 : SBox = 8'b10000010;
             8'b00010010 : SBox = 8'b11001001;
             8'b00010011 : SBox = 8'b01111101;
             8'b00010100 : SBox = 8'b11111010;
             8'b00010101 : SBox = 8'b01011001;
             8'b00010110 : SBox = 8'b01000111;
             8'b00010111 : SBox = 8'b11110000;
             8'b00011000 : SBox = 8'b10101101;
             8'b00011001 : SBox = 8'b11010100;
             8'b00011010 : SBox = 8'b10100010;
             8'b00011011 : SBox = 8'b10101111;
             8'b00011100 : SBox = 8'b10011100;
             8'b00011101 : SBox = 8'b10100100;
             8'b00011110 : SBox = 8'b01110010;
             8'b00011111 : SBox = 8'b11000000;
             8'b00100000 : SBox = 8'b10110111;
             8'b00100001 : SBox = 8'b11111101;
             8'b00100010 : SBox = 8'b10010011;
             8'b00100011 : SBox = 8'b00100110;
             8'b00100100 : SBox = 8'b00110110;
             8'b00100101 : SBox = 8'b00111111;
             8'b00100110 : SBox = 8'b11110111;
             8'b00100111 : SBox = 8'b11001100;
             8'b00101000 : SBox = 8'b00110100;
             8'b00101001 : SBox = 8'b10100101;
             8'b00101010 : SBox = 8'b11100101;
             8'b00101011 : SBox = 8'b11110001;
             8'b00101100 : SBox = 8'b01110001;
             8'b00101101 : SBox = 8'b11011000;
             8'b00101110 : SBox = 8'b00110001;
             8'b00101111 : SBox = 8'b00010101;
             8'b00110000 : SBox = 8'b00000100;
             8'b00110001 : SBox = 8'b11000111;
             8'b00110010 : SBox = 8'b00100011;
             8'b00110011 : SBox = 8'b11000011;
             8'b00110100 : SBox = 8'b00011000;
             8'b00110101 : SBox = 8'b10010110;
             8'b00110110 : SBox = 8'b00000101;
             8'b00110111 : SBox = 8'b10011010;
             8'b00111000 : SBox = 8'b00000111;
             8'b00111001 : SBox = 8'b00010010;
             8'b00111010 : SBox = 8'b10000000;
             8'b00111011 : SBox = 8'b11100010;
             8'b00111100 : SBox = 8'b11101011;
             8'b00111101 : SBox = 8'b00100111;
             8'b00111110 : SBox = 8'b10110010;
             8'b00111111 : SBox = 8'b01110101;
             8'b01000000 : SBox = 8'b00001001;
             8'b01000001 : SBox = 8'b10000011;
             8'b01000010 : SBox = 8'b00101100;
             8'b01000011 : SBox = 8'b00011010;
             8'b01000100 : SBox = 8'b00011011;
             8'b01000101 : SBox = 8'b01101110;
             8'b01000110 : SBox = 8'b01011010;
             8'b01000111 : SBox = 8'b10100000;
             8'b01001000 : SBox = 8'b01010010;
             8'b01001001 : SBox = 8'b00111011;
             8'b01001010 : SBox = 8'b11010110;
             8'b01001011 : SBox = 8'b10110011;
             8'b01001100 : SBox = 8'b00101001;
             8'b01001101 : SBox = 8'b11100011;
             8'b01001110 : SBox = 8'b00101111;
             8'b01001111 : SBox = 8'b10000100;
             8'b01010000 : SBox = 8'b01010011;
             8'b01010001 : SBox = 8'b11010001;
             8'b01010010 : SBox = 8'b00000000;
             8'b01010011 : SBox = 8'b11101101;
             8'b01010100 : SBox = 8'b00100000;
             8'b01010101 : SBox = 8'b11111100;
             8'b01010110 : SBox = 8'b10110001;
             8'b01010111 : SBox = 8'b01011011;
             8'b01011000 : SBox = 8'b01101010;
             8'b01011001 : SBox = 8'b11001011;
             8'b01011010 : SBox = 8'b10111110;
             8'b01011011 : SBox = 8'b00111001;
             8'b01011100 : SBox = 8'b01001010;
             8'b01011101 : SBox = 8'b01001100;
             8'b01011110 : SBox = 8'b01011000;
             8'b01011111 : SBox = 8'b11001111;
             8'b01100000 : SBox = 8'b11010000;
             8'b01100001 : SBox = 8'b11101111;
             8'b01100010 : SBox = 8'b10101010;
             8'b01100011 : SBox = 8'b11111011;
             8'b01100100 : SBox = 8'b01000011;
             8'b01100101 : SBox = 8'b01001101;
             8'b01100110 : SBox = 8'b00110011;
             8'b01100111 : SBox = 8'b10000101;
             8'b01101000 : SBox = 8'b01000101;
             8'b01101001 : SBox = 8'b11111001;
             8'b01101010 : SBox = 8'b00000010;
             8'b01101011 : SBox = 8'b01111111;
             8'b01101100 : SBox = 8'b01010000;
             8'b01101101 : SBox = 8'b00111100;
             8'b01101110 : SBox = 8'b10011111;
             8'b01101111 : SBox = 8'b10101000;
             8'b01110000 : SBox = 8'b01010001;
             8'b01110001 : SBox = 8'b10100011;
             8'b01110010 : SBox = 8'b01000000;
             8'b01110011 : SBox = 8'b10001111;
             8'b01110100 : SBox = 8'b10010010;
             8'b01110101 : SBox = 8'b10011101;
             8'b01110110 : SBox = 8'b00111000;
             8'b01110111 : SBox = 8'b11110101;
             8'b01111000 : SBox = 8'b10111100;
             8'b01111001 : SBox = 8'b10110110;
             8'b01111010 : SBox = 8'b11011010;
             8'b01111011 : SBox = 8'b00100001;
             8'b01111100 : SBox = 8'b00010000;
             8'b01111101 : SBox = 8'b11111111;
             8'b01111110 : SBox = 8'b11110011;
             8'b01111111 : SBox = 8'b11010010;
             8'b10000000 : SBox = 8'b11001101;
             8'b10000001 : SBox = 8'b00001100;
             8'b10000010 : SBox = 8'b00010011;
             8'b10000011 : SBox = 8'b11101100;
             8'b10000100 : SBox = 8'b01011111;
             8'b10000101 : SBox = 8'b10010111;
             8'b10000110 : SBox = 8'b01000100;
             8'b10000111 : SBox = 8'b00010111;
             8'b10001000 : SBox = 8'b11000100;
             8'b10001001 : SBox = 8'b10100111;
             8'b10001010 : SBox = 8'b01111110;
             8'b10001011 : SBox = 8'b00111101;
             8'b10001100 : SBox = 8'b01100100;
             8'b10001101 : SBox = 8'b01011101;
             8'b10001110 : SBox = 8'b00011001;
             8'b10001111 : SBox = 8'b01110011;
             8'b10010000 : SBox = 8'b01100000;
             8'b10010001 : SBox = 8'b10000001;
             8'b10010010 : SBox = 8'b01001111;
             8'b10010011 : SBox = 8'b11011100;
             8'b10010100 : SBox = 8'b00100010;
             8'b10010101 : SBox = 8'b00101010;
             8'b10010110 : SBox = 8'b10010000;
             8'b10010111 : SBox = 8'b10001000;
             8'b10011000 : SBox = 8'b01000110;
             8'b10011001 : SBox = 8'b11101110;
             8'b10011010 : SBox = 8'b10111000;
             8'b10011011 : SBox = 8'b00010100;
             8'b10011100 : SBox = 8'b11011110;
             8'b10011101 : SBox = 8'b01011110;
             8'b10011110 : SBox = 8'b00001011;
             8'b10011111 : SBox = 8'b11011011;
             8'b10100000 : SBox = 8'b11100000;
             8'b10100001 : SBox = 8'b00110010;
             8'b10100010 : SBox = 8'b00111010;
             8'b10100011 : SBox = 8'b00001010;
             8'b10100100 : SBox = 8'b01001001;
             8'b10100101 : SBox = 8'b00000110;
             8'b10100110 : SBox = 8'b00100100;
             8'b10100111 : SBox = 8'b01011100;
             8'b10101000 : SBox = 8'b11000010;
             8'b10101001 : SBox = 8'b11010011;
             8'b10101010 : SBox = 8'b10101100;
             8'b10101011 : SBox = 8'b01100010;
             8'b10101100 : SBox = 8'b10010001;
             8'b10101101 : SBox = 8'b10010101;
             8'b10101110 : SBox = 8'b11100100;
             8'b10101111 : SBox = 8'b01111001;
             8'b10110000 : SBox = 8'b11100111;
             8'b10110001 : SBox = 8'b11001000;
             8'b10110010 : SBox = 8'b00110111;
             8'b10110011 : SBox = 8'b01101101;
             8'b10110100 : SBox = 8'b10001101;
             8'b10110101 : SBox = 8'b11010101;
             8'b10110110 : SBox = 8'b01001110;
             8'b10110111 : SBox = 8'b10101001;
             8'b10111000 : SBox = 8'b01101100;
             8'b10111001 : SBox = 8'b01010110;
             8'b10111010 : SBox = 8'b11110100;
             8'b10111011 : SBox = 8'b11101010;
             8'b10111100 : SBox = 8'b01100101;
             8'b10111101 : SBox = 8'b01111010;
             8'b10111110 : SBox = 8'b10101110;
             8'b10111111 : SBox = 8'b00001000;
             8'b11000000 : SBox = 8'b10111010;
             8'b11000001 : SBox = 8'b01111000;
             8'b11000010 : SBox = 8'b00100101;
             8'b11000011 : SBox = 8'b00101110;
             8'b11000100 : SBox = 8'b00011100;
             8'b11000101 : SBox = 8'b10100110;
             8'b11000110 : SBox = 8'b10110100;
             8'b11000111 : SBox = 8'b11000110;
             8'b11001000 : SBox = 8'b11101000;
             8'b11001001 : SBox = 8'b11011101;
             8'b11001010 : SBox = 8'b01110100;
             8'b11001011 : SBox = 8'b00011111;
             8'b11001100 : SBox = 8'b01001011;
             8'b11001101 : SBox = 8'b10111101;
             8'b11001110 : SBox = 8'b10001011;
             8'b11001111 : SBox = 8'b10001010;
             8'b11010000 : SBox = 8'b01110000;
             8'b11010001 : SBox = 8'b00111110;
             8'b11010010 : SBox = 8'b10110101;
             8'b11010011 : SBox = 8'b01100110;
             8'b11010100 : SBox = 8'b01001000;
             8'b11010101 : SBox = 8'b00000011;
             8'b11010110 : SBox = 8'b11110110;
             8'b11010111 : SBox = 8'b00001110;
             8'b11011000 : SBox = 8'b01100001;
             8'b11011001 : SBox = 8'b00110101;
             8'b11011010 : SBox = 8'b01010111;
             8'b11011011 : SBox = 8'b10111001;
             8'b11011100 : SBox = 8'b10000110;
             8'b11011101 : SBox = 8'b11000001;
             8'b11011110 : SBox = 8'b00011101;
             8'b11011111 : SBox = 8'b10011110;
             8'b11100000 : SBox = 8'b11100001;
             8'b11100001 : SBox = 8'b11111000;
             8'b11100010 : SBox = 8'b10011000;
             8'b11100011 : SBox = 8'b00010001;
             8'b11100100 : SBox = 8'b01101001;
             8'b11100101 : SBox = 8'b11011001;
             8'b11100110 : SBox = 8'b10001110;
             8'b11100111 : SBox = 8'b10010100;
             8'b11101000 : SBox = 8'b10011011;
             8'b11101001 : SBox = 8'b00011110;
             8'b11101010 : SBox = 8'b10000111;
             8'b11101011 : SBox = 8'b11101001;
             8'b11101100 : SBox = 8'b11001110;
             8'b11101101 : SBox = 8'b01010101;
             8'b11101110 : SBox = 8'b00101000;
             8'b11101111 : SBox = 8'b11011111;
             8'b11110000 : SBox = 8'b10001100;
             8'b11110001 : SBox = 8'b10100001;
             8'b11110010 : SBox = 8'b10001001;
             8'b11110011 : SBox = 8'b00001101;
             8'b11110100 : SBox = 8'b10111111;
             8'b11110101 : SBox = 8'b11100110;
             8'b11110110 : SBox = 8'b01000010;
             8'b11110111 : SBox = 8'b01101000;
             8'b11111000 : SBox = 8'b01000001;
             8'b11111001 : SBox = 8'b10011001;
             8'b11111010 : SBox = 8'b00101101;
             8'b11111011 : SBox = 8'b00001111;
             8'b11111100 : SBox = 8'b10110000;
             8'b11111101 : SBox = 8'b01010100;
             8'b11111110 : SBox = 8'b10111011;
             8'b11111111 : SBox = 8'b00010110;
         endcase
     end
     endfunction     
endmodule
