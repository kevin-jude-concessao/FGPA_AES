`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.04.2019 11:25:39
// Design Name: 
// Module Name: MixColumn
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MixColumns(Input, Output);
	input  [0:127] Input;
	output [0:127] Output;
	
	wire [0:7] Wires[32];
	
	GaloisMultiplication mult_00(.A(8'h02), .B(Input[0:7]),  .P(Wires[0]));
    GaloisMultiplication mult_01(.A(8'h02), .B(Input[8:15]), .P(Wires[1]));
    GaloisMultiplication mult_02(.A(8'h02), .B(Input[16:23]),.P(Wires[2]));
    GaloisMultiplication mult_03(.A(8'h02), .B(Input[24:31]),.P(Wires[3]));
    
    GaloisMultiplication mult_04(.A(8'h03), .B(Input[32:39]), .P(Wires[4]));
    GaloisMultiplication mult_05(.A(8'h03), .B(Input[40:47]), .P(Wires[5]));
    GaloisMultiplication mult_06(.A(8'h03), .B(Input[48:55]), .P(Wires[6]));
    GaloisMultiplication mult_07(.A(8'h03), .B(Input[56:63]), .P(Wires[7]));
    
    GaloisMultiplication mult_10(.A(8'h02), .B(Input[32:39]), .P(Wires[8]));
    GaloisMultiplication mult_11(.A(8'h02), .B(Input[40:47]), .P(Wires[9]));
    GaloisMultiplication mult_12(.A(8'h02), .B(Input[48:55]), .P(Wires[10]));
    GaloisMultiplication mult_13(.A(8'h02), .B(Input[56:63]), .P(Wires[11]));
    
    GaloisMultiplication mult_14(.A(8'h03), .B(Input[64:71]), .P(Wires[12]));
    GaloisMultiplication mult_15(.A(8'h03), .B(Input[72:79]), .P(Wires[13]));
    GaloisMultiplication mult_16(.A(8'h03), .B(Input[80:87]), .P(Wires[14]));
    GaloisMultiplication mult_17(.A(8'h03), .B(Input[88:95]), .P(Wires[15]));
    
    GaloisMultiplication mult_20(.A(8'h02), .B(Input[64:71]), .P(Wires[16]));
    GaloisMultiplication mult_21(.A(8'h02), .B(Input[72:79]), .P(Wires[17]));
    GaloisMultiplication mult_22(.A(8'h02), .B(Input[80:87]), .P(Wires[18]));
    GaloisMultiplication mult_23(.A(8'h02), .B(Input[88:95]), .P(Wires[19]));
    
    GaloisMultiplication mult_24(.A(8'h03), .B(Input[96:103]),  .P(Wires[20]));
    GaloisMultiplication mult_25(.A(8'h03), .B(Input[104:111]), .P(Wires[21]));
    GaloisMultiplication mult_26(.A(8'h03), .B(Input[112:119]), .P(Wires[22]));
    GaloisMultiplication mult_27(.A(8'h03), .B(Input[120:127]), .P(Wires[23]));    
    
    GaloisMultiplication mult_30(.A(8'h02), .B(Input[96:103]),  .P(Wires[24]));
    GaloisMultiplication mult_31(.A(8'h02), .B(Input[104:111]), .P(Wires[25]));
    GaloisMultiplication mult_32(.A(8'h02), .B(Input[112:119]), .P(Wires[26]));
    GaloisMultiplication mult_33(.A(8'h02), .B(Input[120:127]), .P(Wires[27]));
    
    GaloisMultiplication mult_34(.A(8'h03), .B(Input[0:7]),   .P(Wires[28]));
    GaloisMultiplication mult_35(.A(8'h03), .B(Input[8:15]),  .P(Wires[29]));
    GaloisMultiplication mult_36(.A(8'h03), .B(Input[16:23]), .P(Wires[30]));
    GaloisMultiplication mult_37(.A(8'h03), .B(Input[24:31]), .P(Wires[31]));
    
	assign Output[0:7]    =  Wires[0] ^ Wires[4] ^ Input[64:71] ^ Input[96:103];
	assign Output[8:15]   =  Wires[1] ^ Wires[5] ^ Input[72:79] ^ Input[104:111];
	assign Output[16:23]  =  Wires[2] ^ Wires[6] ^ Input[80:87] ^ Input[112:119];
	assign Output[24:31]  =  Wires[3] ^ Wires[7] ^ Input[88:95] ^ Input[120:127];
	
	assign Output[32:39]  =  Input[0:7] ^ Wires[8] ^ Wires[12] ^ Input[96:103];
	assign Output[40:47]  =  Input[8:15] ^ Wires[9] ^ Wires[13] ^ Input[104:111]; 
	assign Output[48:55]  =  Input[16:23] ^ Wires[10] ^ Wires[14] ^ Input[112:119];
	assign Output[56:63]  =  Input[24:31] ^ Wires[11] ^ Wires[15] ^ Input[120:127];
	
	assign Output[64:71]  =  Input[0:7] ^ Input[32:39] ^ Wires[16] ^ Wires[20];
	assign Output[72:79]  =  Input[8:15] ^ Input[40:47] ^ Wires[17] ^ Wires[21];
	assign Output[80:87]  =  Input[16:23] ^ Input[48:55] ^ Wires[18] ^ Wires[22];
	assign Output[88:95]  =  Input[24:31] ^ Input[56:63] ^ Wires[19] ^ Wires[23];
	
	assign Output[96:103] =  Wires[28] ^ Input[32:39] ^ Input[64:71] ^ Wires[24];
	assign Output[104:111]=  Wires[29] ^ Input[40:47] ^ Input[72:79] ^ Wires[25];
	assign Output[112:119]=  Wires[30] ^ Input[48:55] ^ Input[80:87] ^ Wires[26];
	assign Output[120:127]=  Wires[31] ^ Input[56:63] ^ Input[88:95] ^ Wires[27];
	
endmodule