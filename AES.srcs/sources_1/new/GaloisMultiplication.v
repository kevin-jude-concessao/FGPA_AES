`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2019 19:12:10
// Design Name: 
// Module Name: GaloisMultiplication
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GaloisMultiplication(A, B, P);
input  [0:7] A;
input  [0:7] B;
output [0:7] P;

wire  [0:7] A_Wires[8];
wire  [0:7] B_Wires[8];
wire  [0:7] P_Wires[7];

GaloisOneStep step_1(.OldA(A), .OldB(B), .OldP(8'b00000000), .NewA(A_Wires[0]), .NewB(B_Wires[0]), .NewP(P_Wires[0]));
GaloisOneStep step_2(.OldA(A_Wires[0]), .OldB(B_Wires[0]), .OldP(P_Wires[0]), .NewA(A_Wires[1]), .NewB(B_Wires[1]), .NewP(P_Wires[1]));
GaloisOneStep step_3(.OldA(A_Wires[1]), .OldB(B_Wires[1]), .OldP(P_Wires[1]), .NewA(A_Wires[2]), .NewB(B_Wires[2]), .NewP(P_Wires[2]));
GaloisOneStep step_4(.OldA(A_Wires[2]), .OldB(B_Wires[2]), .OldP(P_Wires[2]), .NewA(A_Wires[3]), .NewB(B_Wires[3]), .NewP(P_Wires[3]));
GaloisOneStep step_5(.OldA(A_Wires[3]), .OldB(B_Wires[3]), .OldP(P_Wires[3]), .NewA(A_Wires[4]), .NewB(B_Wires[4]), .NewP(P_Wires[4]));
GaloisOneStep step_6(.OldA(A_Wires[4]), .OldB(B_Wires[4]), .OldP(P_Wires[4]), .NewA(A_Wires[5]), .NewB(B_Wires[5]), .NewP(P_Wires[5]));
GaloisOneStep step_7(.OldA(A_Wires[5]), .OldB(B_Wires[5]), .OldP(P_Wires[5]), .NewA(A_Wires[6]), .NewB(B_Wires[6]), .NewP(P_Wires[6]));
GaloisOneStep step_8(.OldA(A_Wires[6]), .OldB(B_Wires[6]), .OldP(P_Wires[6]), .NewA(A_Wires[7]), .NewB(B_Wires[7]), .NewP(P));
endmodule
