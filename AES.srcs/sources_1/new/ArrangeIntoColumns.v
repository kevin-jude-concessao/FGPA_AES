`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2019 13:35:25
// Design Name: 
// Module Name: ArrangeIntoColumns
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ArrangeIntoColumns(Input, Output);
input  [0:127] Input;
output [0:127] Output;

assign Output[0:7]     = Input[0:7];
assign Output[32:39]   = Input[8:15];
assign Output[64:71]   = Input[16:23];
assign Output[96:103]  = Input[24:31];

assign Output[8:15]    = Input[32:39];
assign Output[40:47]   = Input[40:47];
assign Output[72:79]   = Input[48:55];
assign Output[104:111] = Input[56:63];

assign Output[16:23]   = Input[64:71];
assign Output[48:55]   = Input[72:79];
assign Output[80:87]   = Input[80:87];
assign Output[112:119] = Input[88:95];

assign Output[24:31]   = Input[96:103];
assign Output[56:63]   = Input[104:111];
assign Output[88:95]   = Input[112:119];
assign Output[120:127] = Input[120:127];

endmodule